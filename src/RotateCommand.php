<?php namespace Partitionator;

use Exception;
use RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Helper\Table;
use Partitionator\AbstractCommand;
use Partitionator\Partition;
use Partitionator\StatusCommand;

class RotateCommand extends AbstractCommand
{
	/**
	 * @var string Command name description
	 */
	protected static $defaultName = 'rotate';

	/**
	 * Configure the command
	 *
	 * @return void
	 */
	protected function configure()
	{
		$this->setDescription('Rotate the partitions for the given table')
			->setHelp('This command allows you to rotate existing partitions')
			->addOption("table_name", 'i', InputOption::VALUE_REQUIRED ,'Table name');
	}

	/**
	 * Interacts with the user.
	 *
	 * This method is executed before the InputDefinition is validated.
	 * This means that this is the only place where the command can
	 * interactively ask for values of missing required arguments.
	 */
	protected function interact(InputInterface $input, OutputInterface $output)
	{
		try
		{
			if (!$input->getOption('table_name'))
			{
				throw new RuntimeException('Invalid --table_name [-i] option');
			}

			$this->verbose = $input->getOption('verbose') ? true : false;
			$this->output = $output;
		}
		catch (RuntimeException $e)
		{
			$output->writeln('<error>' . $e->getMessage());
			exit;
		}
	}

	/**
	 * Execute the command
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @return void
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		try
		{
			$partitions = $this->getPartitionNames($input->getOption('table_name'));

			if (!$this->hasStartFuturePartitions($partitions))
			{
				throw new RuntimeException('Missing [start, future] partitions for table ' . $input->getOption('table_name'));
			}

			$lastPartition = $this->findLastPartition($partitions);

			if (!$lastPartition)
			{
				throw new RuntimeException('The given table does not have an existing period partition.');
			}

			$deleteds = 0;

			$newPartition = $this->rotateFromLastPartition($input->getOption('table_name'), $lastPartition);

			// Only if we already created a new partition we drop the oldest one
			if ($newPartition)
			{
				$allowedToDeletePartitions = array_reverse(array_filter($partitions, function($partition)
				{
					return !in_array($partition, [Partition::PARTITION_FUTURE, Partition::PARTITION_START]);
				}));

				$this->dropPartitions($input->getOption('table_name'), [$allowedToDeletePartitions[0]]);
				$deleteds = 1;
			}

			$this->printResults($input, $output, $newPartition, $deleteds);
		}
		catch (Exception $e)
		{
			$output->writeln('<error>' . $e->getMessage());
		}
	}

	/**
	 * Print results in a beautiful way
	 *
	 * @param InputInterface  $input
	 * @param OutputInterface $output
	 * @param Partition|null  $partition
	 * @param integer         $deleteds
	 * @return void
	 */
	private function printResults(InputInterface $input, OutputInterface $output, ?Partition $partition, int $deleteds) : void
	{
		if ($partition)
		{
			$output->writeln('New partition: ');
			$table = new Table($output);
			$table->setHeaders(['Name', 'Until date'])->setRows([
				[$partition->getName(), $partition->getUntilDate()->format('Y-m-d')]
			]);
			$table->render();
		}

		if ($output->isVerbose())
		{
			$table = new Table($output);
			$table->setHeaders(['Table', 'Type', 'Partitions'])->setRows(StatusCommand::getPartitionsInfo($this->getAllPartitions()));
			$table->render();
		}

		if ($deleteds > 0)
		{
			$output->writeln('');
			$output->writeln('Dropped Partitions: ' . $deleteds);
		}
	}
}