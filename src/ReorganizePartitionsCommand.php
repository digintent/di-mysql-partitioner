<?php namespace Partitionator;

use Exception;
use InvalidArgumentException;
use RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Helper\Table;
use Partitionator\AbstractCommand;
use Partitionator\Partition;

class ReorganizePartitionsCommand extends AbstractCommand
{
	/**
	 * @var string Command name description
	 */
	protected static $defaultName = 'reorganize';

	/**
	 * Configure the command
	 *
	 * @return void
	 */
	protected function configure()
	{
		$this->setDescription('Reorganize partitions for the given table')
			->setHelp('This command allows you to reorganize existing partitions in an existing table')
			->addOption("table_name", 'i', InputOption::VALUE_REQUIRED ,'Table name')
			->addOption("partitions", 'p', InputOption::VALUE_REQUIRED ,'Number of partitions')
			->addOption("period_time", "t", InputOption::VALUE_REQUIRED, 'Time period to reorganize the table');
	}

	/**
	 * Interacts with the user.
	 *
	 * This method is executed before the InputDefinition is validated.
	 * This means that this is the only place where the command can
	 * interactively ask for values of missing required arguments.
	 */
	protected function interact(InputInterface $input, OutputInterface $output)
	{
		try
		{
			if (!$input->getOption('table_name'))
			{
				throw new RuntimeException('Invalid --table_name [-i] option');
			}

			if (!$input->getOption('partitions'))
			{
				throw new RuntimeException('Invalid --partitions [-p] option');
			}

			if (!$input->getOption('period_time'))
			{
				throw new RuntimeException('Invalid --period_time [-t] option');
			}

			if (!in_array($input->getOption('period_time'), Partition::ALLOWED_PERIOD_TIME))
			{
				throw new RuntimeException('Invalid period_time. Allowed values: ' . implode(', ', Partition::ALLOWED_PERIOD_TIME));
			}

			$this->output = $output;
		}
		catch (RuntimeException $e)
		{
			$output->writeln('<error>' . $e->getMessage());
			exit;
		}
	}

	/**
	 * Execute the command
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @return void
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		try
		{
			$partitions = $this->getPartitionNames($input->getOption('table_name'));

			if (!$this->hasStartFuturePartitions($partitions))
			{
				throw new RuntimeException('Missing [start, future] partitions for table ' . $input->getOption('table_name'));
			}

			$deleteds = 0;

			$newPartition = $this->rotate($input->getOption('table_name'), $input->getOption('period_time'), $this->findLastPartition($partitions));

			// Only if we already created a new partition we drop the oldest one
			if ($newPartition)
			{
				$allowedToDeletePartitions = array_filter($partitions, function($partition)
				{
					return !in_array($partition, [Partition::PARTITION_FUTURE, Partition::PARTITION_START]);
				});

				if (count($allowedToDeletePartitions) >= (int)$input->getOption('partitions'))
				{
					$partitionsToDelete = array_slice(array_reverse($allowedToDeletePartitions), 0, count($allowedToDeletePartitions) - (int)$input->getOption('partitions'));
					$deleteds = $this->dropPartitions($input->getOption('table_name'), $partitionsToDelete);
				}
			}

			$this->printResults($input, $output, $newPartition, $deleteds);
		}
		catch (Exception $e)
		{
			$output->writeln('<error>' . $e->getMessage());
		}
	}

	/**
	 * Print results in a beautiful way
	 *
	 * @param InputInterface  $input
	 * @param OutputInterface $output
	 * @param Partition|null  $partition
	 * @param integer         $deleteds
	 * @return void
	 */
	private function printResults(InputInterface $input, OutputInterface $output, ?Partition $partition, int $deleteds) : void
	{
		if ($partition)
		{
			$output->writeln('New partition: ');
			$table = new Table($output);
			$table->setHeaders(['Name', 'Until date'])->setRows([
				[$partition->getName(), $partition->getUntilDate()->format('Y-m-d')]
			]);
			$table->render();
		}

		if ($output->isVerbose())
		{
			$table = new Table($this->output);
			$table->setHeaders(['Table', 'Type', 'Partitions'])->setRows(StatusCommand::getPartitionsInfo($this->getAllPartitions()));
			$table->render();
		}

		if ($deleteds > 0)
		{
			$output->writeln('');
			$output->writeln('Dropped Partitions: ' . $deleteds);
		}
	}

	/**
	 * Rotate if needed the existing partitions
	 *
	 * @param string          $tableName      Table name
	 * @param string          $period         Period time
	 * @param Partition|null  $lastPartition  Last partition
	 * @return Partition|null
	 */
	private function rotate(string $tableName, string $period, ?Partition $lastPartition) : ?Partition
	{
		if ($lastPartition && $lastPartition->getPeriod() !== $period)
		{
			throw new InvalidArgumentException('Selected period [' . $period . '] must follow the existing partition definition: [' . $lastPartition->getPeriod() . ']');
		}

		return $lastPartition === null ?
			$this->createFirstPartition($tableName, $period) :
			$this->rotateFromLastPartition($tableName, $lastPartition);
	}
}