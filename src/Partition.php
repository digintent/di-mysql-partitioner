<?php namespace Partitionator;

use DateInterval;
use DateTimeImmutable;
use InvalidArgumentException;

/**
 * Class to handle partition schema
 */
class Partition
{
	const PARTITION_START = 'start';
	const PARTITION_FUTURE = 'future';
	const PERIOD_DAILY = 'daily';
	const PERIOD_WEEKLY = 'weekly';
	const PERIOD_MONTHLY = 'monthly';
	const PERIOD_YEARLY = 'yearly';

	const ALLOWED_PERIOD_TIME = [
		self::PERIOD_DAILY,
		self::PERIOD_WEEKLY,
		self::PERIOD_MONTHLY,
		self::PERIOD_YEARLY,
	];

	const PERIOD_INTERVAL_MAP = [
		self::PERIOD_DAILY => 'D',
		self::PERIOD_WEEKLY => 'W',
		self::PERIOD_MONTHLY => 'M',
		self::PERIOD_YEARLY => 'Y',
	];

	/**
	 * @var string Partition database name
	 */
	protected $name;

	/**
	 * Constructs the class
	 *
	 * @param string $name  Partition database name in the allowed formats: [fromY, fromYm, fromYmd, fromYwW]
	 */
	public function __construct(string $name)
	{
		$this->name = $name;
	}

	/**
	 * Get partition name
	 *
	 * @return string
	 */
	public function getName() : string
	{
		return $this->name;
	}

	/**
	 * Compute the period from the partition name
	 *
	 * @return string
	 */
	public function getPeriod() : string
	{
		$name = str_replace('from', '', $this->name);

		if (strlen($name) === 4)
		{
			return self::PERIOD_YEARLY;
		}

		if (strlen($name) === 6)
		{
			return self::PERIOD_MONTHLY;
		}

		if (strlen($name) === 8)
		{
			return self::PERIOD_DAILY;
		}

		if (strpos($name, 'w') > 0)
		{
			return self::PERIOD_WEEKLY;
		}

		throw new InvalidArgumentException('Partition with name ' . $this->name .
			' is not configured properly with the allowed periods: ' . implode(',', self::ALLOWED_PERIOD_TIME)
		);
	}

	/**
	 * Compute the related date from the partition name
	 *
	 * @return DateTimeInterface
	 */
	public function getDateFromName() : DateTimeImmutable
	{
		$dateStr = str_replace('from', '', $this->name);

		switch ($this->getPeriod())
		{
			case self::PERIOD_YEARLY:
				$date = new DateTimeImmutable($dateStr . '-01-01');
			break;
			case self::PERIOD_MONTHLY:
				$year = substr($dateStr, 0, 4);
				$month = substr($dateStr, 4, 2);
				$date = new DateTimeImmutable($year . '-' . $month . '-01') ;
			break;
			case self::PERIOD_DAILY:
				$date = new DateTimeImmutable($dateStr) ;
			break;
			case self::PERIOD_WEEKLY:
				[$year, $week] = explode('w', $dateStr);
				$date = (new DateTimeImmutable())->setISODate($year, $week);
			break;
		}

		return $date;
	}

	/**
	 * Get until date from partition
	 *
	 * @return DateTimeImmutable
	 */
	public function getUntilDate() : DateTimeImmutable
	{
		$partitionDate = $this->getDateFromName();

		switch ($this->getPeriod())
		{
			case self::PERIOD_YEARLY:
				$date = new DateTimeImmutable($partitionDate->add(new DateInterval('P1Y'))->format('Y-') . '01-01');
			break;
			case self::PERIOD_MONTHLY:
				$date = new DateTimeImmutable($partitionDate->add(new DateInterval('P1M'))->format('Y-m-') . '01');
			break;
			case self::PERIOD_DAILY:
				$date = $partitionDate->add(new DateInterval('P1D'));
			break;
			case self::PERIOD_WEEKLY:
				$date = $partitionDate->modify('Monday next week 00:00');
			break;
		}

		return $date;
	}

	/**
	 * Create a new partition from a date and a defined period
	 *
	 * @param DateTimeImmutable $date     Date related to the partition
	 * @param string            $period   Time period defined to the partition
	 * @return Partition
	 */
	public static function fromDateAndPeriod(DateTimeImmutable $date, string $period) : Partition
	{
		$partitionName = 'from';

		switch ($period)
		{
			case self::PERIOD_YEARLY:
				$partitionName .= $date->add(new DateInterval('P1Y'))->format('Y');
			break;
			case self::PERIOD_MONTHLY:
				$partitionName .= $date->add(new DateInterval('P1M'))->format('Ym');
			break;
			case self::PERIOD_DAILY:
				$partitionName .= $date->add(new DateInterval('P1D'))->format('Ymd');
			break;
			case self::PERIOD_WEEKLY:
				$partitionName .= $date->modify('Monday next week 00:00')->format('o\wW');
			break;
		}

		return new self($partitionName);
	}

	/**
	 * Compute the next Partition from the current one
	 *
	 * @return Partition
	 */
	public function getNext() : Partition
	{
		return self::fromDateAndPeriod($this->getDateFromName(), $this->getPeriod());
	}

	/**
	 * Returns an string representation of the partition instance
	 *
	 * @return string
	 */
	public function __toString()
	{
		return json_encode([
			'name' => $this->name,
			'date_from_name' => $this->getDateFromName()->format('Y-m-d'),
			'period' => $this->getPeriod(),
			'until' => $this->getUntilDate()->format('Y-m-d'),
		]);
	}
}