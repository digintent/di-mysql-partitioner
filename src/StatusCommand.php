<?php namespace Partitionator;

use Exception;
use InvalidArgumentException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Helper\Table;
use Partitionator\AbstractCommand;
use Partitionator\Partition;

class StatusCommand extends AbstractCommand
{
	const MAX_SHOWED_PARTITIONS = 14;

	/**
	 * @var string Command name description
	 */
	protected static $defaultName = 'status';

	/**
	 * Configure the command
	 *
	 * @return void
	 */
	protected function configure()
	{
		$this->setDescription('Reorganize partitions for the given table')
			->setHelp('This command allows you to reorganize existing partitions in an existing table')
			->addOption("table_name", 'i', InputOption::VALUE_REQUIRED ,'Table name');
	}

	/**
	 * Execute the command
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @return void
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		try
		{
			$this->output = $output;

			$partitions = [];

			if ($input->getOption('table_name'))
			{
				$partitions[$input->getOption('table_name')] = $this->getPartitionNames($input->getOption('table_name'));
			}
			else
			{
				$partitions = $this->getAllPartitions();
			}

			$this->printResults($partitions);
		}
		catch (Exception $e)
		{
			$output->writeln('<error>' . $e->getMessage());
		}
	}

	/**
	 * Get partitions info
	 *
	 * @param array $items
	 * @return array
	 */
	public static function getPartitionsInfo(array $items) : array
	{
		$tableRows = [];

		foreach ($items as $tableName => $partitions)
		{
			if (count($partitions) > 0)
			{
				$firstOne = str_replace('from', '', $partitions[0]);

				// Empty partitioned table
				if (count($partitions) === 2 && ($firstOne === Partition::PARTITION_FUTURE || $firstOne === Partition::PARTITION_START))
				{
					$tableRows[] = [$tableName, 'Empty', ''];
				}
				else
				{
					$type = null;
					$partitionNames = [];
					foreach ($partitions as $pName)
					{
						try
						{
							$type = (new Partition($pName))->getPeriod();
							$partitionNames[] = str_replace('from', '', $pName);
						}
						catch (InvalidArgumentException $e)
						{
							$type = 'Unsupported';
							$partitionNames[] = $pName;
						}
					}

					$partitionNames = array_reverse(array_filter($partitionNames, function($pName)
					{
						return !in_array($pName, [Partition::PARTITION_START, Partition::PARTITION_FUTURE]);
					}));

					if (count($partitionNames) > self::MAX_SHOWED_PARTITIONS)
					{
						$split = self::MAX_SHOWED_PARTITIONS/2;
						$start = array_slice($partitionNames, 0, $split);
						$end = array_slice($partitionNames, count($partitionNames) - $split, $split);
						$partitionToShow = implode(', ', $start) . ' ... ' . implode(', ', $end);
					}
					else
					{
						$partitionToShow = implode(', ', $partitionNames);
					}

					$tableRows[] = [$tableName, $type, $partitionToShow];
				}
			}
		}

		return $tableRows;
	}

	/**
	 * Print results in a beautiful way
	 *
	 * @param array $partition
	 * @return void
	 */
	private function printResults(array $items) : void
	{
		$table = new Table($this->output);
		$table->setHeaders(['Table', 'Type', 'Partitions'])->setRows(self::getPartitionsInfo($items));
		$table->render();
	}
}