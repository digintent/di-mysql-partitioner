<?php namespace Partitionator;

use DateTimeImmutable;
use PDO;
use Symfony\Component\Console\Command\Command;
use Partitionator\Partition;
use PDOStatement;
use Symfony\Component\Console\Output\OutputInterface;

class AbstractCommand extends Command
{
	/**
	 * @var PDO Databahase handle
	 */
	protected $database;

	/**
	 * @var \Symfony\Component\Console\Output\OutputInterface
	 */
	protected $output;

	/**
	 * @var array Database Config info
	 */
	protected $config;

	/**
	 * @var boolean Flag to determine when to dry run
	 */
	protected $dryRun = false;

	/**
	 * Constructs the command
	 *
	 * @param PDO $pdo  PDO instance
	 */
	public function __construct(PDO $database, array $config)
	{
		parent::__construct();
		$this->database = $database;
		$this->config = $config;
	}

	/**
	 * Print put the SQL statements if the verbose level was selected
	 *
	 * @param string $message
	 * @param PDOStatement $stm
	 * @return void
	 */
	public function debug(string $message, PDOStatement $stm)
	{
		if ($this->output->getVerbosity() === OutputInterface::VERBOSITY_DEBUG)
		{
			$this->output->writeln('<info> ' . $message . ' start');
			$stm->debugDumpParams();
		}
	}

	/**
	 * Drop the given partitions
	 *
	 * @param string $tableName    Table name
	 * @param array  $partitions   List of partition names
	 * @return int
	 */
	public function dropPartitions(string $tableName, array $partitions) : int
	{
		$deleted = 0;

		foreach($partitions as $partition)
		{
			if ($partition !== Partition::PARTITION_FUTURE && $partition !== Partition::PARTITION_START)
			{
				$stm = $this->database->prepare("ALTER TABLE $tableName DROP PARTITION $partition");

				if (!$this->dryRun)
				{
					$stm->execute();
					$deleted++;
				}

				$this->debug('DROP Partition', $stm);
			}
		}

		return $deleted;
	}

	/**
	 * Check if the given table has the required partitions
	 *
	 * @param string $tableName
	 * @return boolean
	 */
	public function hasStartFuturePartitions(array $partitions) : bool
	{
		return count(array_filter($partitions, function($partition)
		{
			return $partition === Partition::PARTITION_START || $partition === Partition::PARTITION_FUTURE;
		})) === 2;
	}

	/**
	 * Get partition info from table
	 *
	 * @param string $tableName
	 * @return array List of existing partition names
	 */
	public function getPartitionNames(string $tableName) : array
	{
		$stm = $this->database->prepare('SELECT PARTITION_NAME
			FROM INFORMATION_SCHEMA.PARTITIONS
			WHERE TABLE_NAME=:table_name
			AND table_schema=:table_schema
			ORDER BY PARTITION_NAME DESC');

		$stm->bindValue('table_name', $tableName);
		$stm->bindValue('table_schema', $this->config['db_name']);
		$stm->execute();

		$partitions = $stm->fetchAll(PDO::FETCH_ASSOC);

		return array_filter(array_column($partitions, 'PARTITION_NAME'));
	}


	/**
	 * Fetch all the partitions form a given schema
	 *
	 * @return array
	 */
	public function getAllPartitions() : array
	{
		$stm = $this->database->prepare('SELECT TABLE_NAME, PARTITION_NAME
			FROM INFORMATION_SCHEMA.PARTITIONS
			WHERE table_schema=:table_schema
			AND PARTITION_NAME IS NOT NULL
			ORDER BY PARTITION_NAME DESC');

		$stm->bindValue('table_schema', $this->config['db_name']);
		$stm->execute();

		$items = $stm->fetchAll(PDO::FETCH_ASSOC);

		$partitions = [];

		foreach ($items as $item)
		{
			if (!isset($partitions[$item['TABLE_NAME']]))
			{
				$partitions[$item['TABLE_NAME']] = [];
			}

			$partitions[$item['TABLE_NAME']][] = $item['PARTITION_NAME'];
		}

		return $partitions;
	}

	/**
	 * Add a new partition to the existing table
	 *
	 * @param string    $tableName   Table name
	 * @param Partition $partition   Partition object
	 * @return void
	 */
	public function addPartition(string $tableName, Partition $partition) : void
	{
		$newPartitionName = $partition->getName();
		$untilDate = $partition->getUntilDate()->format('Y-m-d');

		$stm = $this->database->prepare("ALTER TABLE $tableName
			REORGANIZE PARTITION future INTO (
				PARTITION $newPartitionName VALUES LESS THAN (TO_DAYS('$untilDate')),
				PARTITION future VALUES LESS THAN MAXVALUE
			)"
		);

		if (!$this->dryRun)
		{
			$stm->execute();
		}

		$this->debug('REORGANIZE Partition', $stm);
	}

	/**
	 * Find the last partition from the given list
	 *
	 * @param array $partitions
	 * @return Partition|null
	 */
	public function findLastPartition(array $partitions) : ?Partition
	{
		$last = null;

		foreach ($partitions as $partition)
		{
			if ($partition !== Partition::PARTITION_START && $partition !== Partition::PARTITION_FUTURE)
			{
				$last = new Partition($partition);
				break;
			}
		}

		return $last;
	}

	/**
	 * It will try to create a new partition from the existing one if within the required timeframe
	 *
	 * @param Partition $partition
	 * @param integer $minutes
	 * @return Partition|null
	 */
	public function rotateFromLastPartition(string $tableName, Partition $partition, int $minutes = 15) : ?Partition
	{
		$newPartition = null;
		$startDt = new DateTimeImmutable('now UTC');

		if ($partition->getDateFromName() > $startDt)
		{
			// If the last partition will start in the next 15 minutes, we create the new one in the future
			// Or if this is the first partition, we just create it
			$minuteDiff = ($partition->getDateFromName()->setTime(0, 0, 0)->getTimestamp() - $startDt->getTimestamp()) / 60;

			if ($minuteDiff <= 15 || true)
			{
				$newPartition = $partition->getNext();
				$this->addPartition($tableName, $newPartition);
			}
		}

		return $newPartition;
	}

	/**
	 * Create the first period partition
	 *
	 * @param string $tableName    Table name
	 * @param string $period       Time period
	 * @return Partition
	 */
	public function createFirstPartition(string $tableName, string $period) :Partition
	{
		$newPartition = Partition::fromDateAndPeriod(new DateTimeImmutable('now UTC'), $period);
		$this->addPartition($tableName, $newPartition);

		return $newPartition;
	}
}