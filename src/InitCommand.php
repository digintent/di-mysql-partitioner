<?php namespace Partitionator;

use DateInterval;
use DatePeriod;
use DateTimeImmutable;
use Exception;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Helper\Table;
use Partitionator\AbstractCommand;
use Partitionator\Partition;
use RuntimeException;

class InitCommand extends AbstractCommand
{
	/**
	 * @var string Command name description
	 */
	protected static $defaultName = 'init';

	/**
	 * Configure the command
	 *
	 * @return void
	 */
	protected function configure()
	{
		$this->setDescription('Configure the table with the partition schema')
			->setHelp('This command allows you to configure the table with the partition schema')
			->addOption("table_name", 'i', InputOption::VALUE_REQUIRED ,'Table name')
			->addOption("key", 'k', InputOption::VALUE_REQUIRED ,'Key usted for the partition')
			->addOption("partitions", 'p', InputOption::VALUE_REQUIRED ,'Number of partitions')
			->addOption("period_time", "t", InputOption::VALUE_REQUIRED, 'Time period to reorganize the table')
			->addOption("dry-run", 'd', InputOption::VALUE_NONE ,'Dry run');
	}

	/**
	 * Interacts with the user.
	 *
	 * This method is executed before the InputDefinition is validated.
	 * This means that this is the only place where the command can
	 * interactively ask for values of missing required arguments.
	 */
	protected function interact(InputInterface $input, OutputInterface $output)
	{
		try
		{
			if (!$input->getOption('table_name'))
			{
				throw new RuntimeException('Invalid --table_name [-i] option');
			}

			if (!$input->getOption('key'))
			{
				throw new RuntimeException('Invalid --key [-k] option');
			}

			if (!$input->getOption('partitions'))
			{
				throw new RuntimeException('Invalid --partitions [-p] option');
			}

			if (!$input->getOption('period_time'))
			{
				throw new RuntimeException('Invalid --period_time [-t] option');
			}

			if (!in_array($input->getOption('period_time'), Partition::ALLOWED_PERIOD_TIME))
			{
				throw new RuntimeException('Invalid period_time. Allowed values: ' . implode(', ', Partition::ALLOWED_PERIOD_TIME));
			}

			$this->dryRun = (boolean)$input->getOption('dry-run');
			$this->output = $output;
		}
		catch (RuntimeException $e)
		{
			$output->writeln('<error>' . $e->getMessage());
			exit;
		}
	}

	/**
	 * Execute the command
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @return void
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		try
		{
			$this->configureTable(
				$input->getOption('table_name'),
				$input->getOption('key'),
				(int)$input->getOption('partitions'),
				$input->getOption('period_time')
			);

			$table = new Table($this->output);
			$table->setHeaders(['Table', 'Type', 'Partitions'])->setRows(StatusCommand::getPartitionsInfo([
				$input->getOption('table_name') => $this->getPartitionNames($input->getOption('table_name')),
			]));
			$table->render();
		}
		catch (Exception $e)
		{
			$output->writeln('<error>' . $e->getMessage());
		}
	}

	/**
	 * Configure the given table with the selected partition schema
	 *
	 * @param string   $tableName     Table name
	 * @param string   $key           Table column to use as key
	 * @param integer  $nPartitions   Number of partitions
	 * @param string   $period        Period to create partitions
	 * @return void
	 */
	private function configureTable(string $tableName, string $key, int $nPartitions, string $period)
	{
		if ($this->getPartitionNames($tableName) !== [])
		{
			throw new RuntimeException('The table ' . $tableName . ' is already partitioned.');
		}

		$partitions = [];

		$interval = Partition::PERIOD_INTERVAL_MAP[$period];
		$endDate = new DateTimeImmutable('now UTC');
		$startDate = $endDate->sub(new DateInterval('P' . $nPartitions . $interval));

		foreach (new DatePeriod($startDate, new DateInterval('P1' . $interval), $endDate) as $date)
		{
			$partition = Partition::fromDateAndPeriod($date, $period);
			$newPartitionName = $partition->getName();
			$untilDate = $partition->getUntilDate()->format('Y-m-d');
			$partitions[] = "PARTITION $newPartitionName VALUES LESS THAN (TO_DAYS('$untilDate'))";
		}

		$partitions = implode(",\n", $partitions);

		$stm = $this->database->prepare("ALTER TABLE $tableName PARTITION BY RANGE (TO_DAYS($key))
		(
			PARTITION start VALUES LESS THAN(0),
			$partitions,
			PARTITION future VALUES LESS THAN MAXVALUE
		)");

		if (!$this->dryRun)
		{
			$stm->execute();
		}

		$this->debug('Initialize table ' . $tableName, $stm);
	}
}