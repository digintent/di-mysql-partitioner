Digital Intent mysql partitioner
====

**How to use**

Example
`php partitionator reorganize -iidentity_daily_pause -p10 -tmonthly --verbose`

Output
```

New partition: 
+------------+------------+
| Name       | Until date |
+------------+------------+
| from202004 | 2020-05-01 |
+------------+------------+
+------------+
| Partitions |
+------------+
| start      |
| future     |
| from202004 |
| from202003 |
| from202002 |
| from202001 |
| from201912 |
+------------+
```

Options
 TODO